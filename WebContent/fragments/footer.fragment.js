sap.ui.jsfragment("fragments.footer", {
    
    setLeadingZero : function(i) {
        return (i<10)? "0"+i : i;
    },
    
    liveClockChange: function(timeObj){
        var that = this;
        var today = new Date();
        var h = this.setLeadingZero(today.getHours());
        var m = this.setLeadingZero(today.getMinutes());
        var s = this.setLeadingZero(today.getSeconds());
    
       setTimeout(function(){that.liveClockChange(timeObj)}, 500);
              
       timeObj.setText(h + ":" + m + ":" + s);        
    },
    
    createContent: function (oController) {
        var prefix = oController.prefix;
        var versionNumber = new sap.m.Text({
            id : prefix+"versionNumber",
            text : "Version 0.0.2"
        });
        
        var author = new sap.m.Text({
            id : prefix+"author",
            text : "Cpt.Dain und Cmd.Till"
        });
        
        var liveClock = new sap.m.Label({
            id : prefix + "liveClock",
            text : "00:00:00"
        });
        
        this.liveClockChange(liveClock);
         
        return new sap.m.Bar({
                                contentLeft : [author],
                                contentMiddle : [versionNumber],
                                contentRight : [liveClock]
        });
    }
});

