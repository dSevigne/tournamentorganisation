neusta.Router = [

                    {
                        pattern: ["", "home", "initialView"], //url statements
                        name: "Home",
                        view: "InitialView",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "root_app", //dahin wirds geladen
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){ //gets triggered when url changes
                            app.to("home"); //Id der Page, s.o.
                        }
                    },
                    {
                        pattern: "userlist",
                        name: "Userlist",
                        view: "Userlist",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "root_app",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("userlist");
                        }
                    },
                    {
                        pattern: "userlistmaster",
                        name: "UserListMaster",
                        view: "UserListMaster",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "root_app",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("rawSplitApp");
                            rawSplitApp = sap.ui.getCore().byId("RawSplitApp_rawSplitApp");
                            userListMasterPage = sap.ui.getCore().byId("RawSplitApp_userListMasterPage");
                            rawSplitApp.toMaster(userListMasterPage);
                            
                        }
                    },
                    {
                        pattern: "rawsplitapp",
                        name: "RawSplitApp",
                        view: "rawSplitApp",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "root_app",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("rawSplitApp");
                        }
                    },
                    {
                        pattern: "currenttourneys",
                        name: "TourneyMaster",
                        view: "TourneyMaster",
                        viewType: sap.ui.core.mvc.ViewType.JS,
                        targetControl: "root_app",
                        //targetAggregation: "page",
                        clearTarget: true,
                        callback: function(){
                            app.to("tourneyMaster");
                        }
                    }
];