function parseJsonToXml(object){
	
	 var jsonModel = sap.ui.getCore().getModel('equipmentsModel');	 
	  jsonDataArray = jsonModel.getData().equipments.item;
	  
	 var allEquipmentNumbers = [];
	 for (var i = 0; i<jsonDataArray.length; i++){
		 allEquipmentNumbers.push(jsonDataArray[i].number);
	 };
	 
	 var xmlString = '<?xml version="1.0" encoding="UTF-8"?><equipmentErrors>';
	 
	 for (var i = 0; i<allEquipmentNumbers.length; i++){
		 xmlString = xmlString + '<item><equipmentNumber>' + allEquipmentNumbers[i] + '</equipmentNumber><errors /></item>';
	 };
	 
	 xmlString = xmlString + '</equipmentErrors>';
	 
	 console.log(xmlString);
};