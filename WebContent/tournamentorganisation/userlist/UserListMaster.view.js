sap.ui.jsview("tournamentorganisation.userlist.UserListMaster", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf tournamentorganisation.userlist.UserListMaster
	*/ 
	getControllerName : function() {
		return "tournamentorganisation.userlist.UserListMaster";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf tournamentorganisation.userlist.UserListMaster
	*/ 
	createContent : function(oController) {
		var prefix = oController.prefix;
		
		var userListMaster = new sap.m.List(prefix+"userListMaster", {
			mode : sap.m.ListMode.SingleSelectMaster,
			select : function(oEvent){
				oController.itemSelected(oEvent.getSource());
			}
		});
		
		var userListMasterItemTemplate = new sap.m.StandardListItem(prefix+"userListMasterItemTemplate", {
			title : "{userListModel>userName}",
			description : "{userListModel>firstName}"
		});
		
		//items --> an diese property von userListMaster wird gebunden
		userListMaster.bindAggregation("items",
				"userListModel>/users", userListMasterItemTemplate);
		
		var userListMasterPage = new sap.m.Page(prefix+"userListMasterPage",{
			content: [userListMaster]
		});
		
 		return userListMasterPage;
	}

});