sap.ui.controller("tournamentorganisation.userlist.Userlist", {
    prefix: "Userlist_",
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf tournamentorganisation.Userlist
*/
//	onInit: function() {
//
//	},
	
	
/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf tournamentorganisation.Userlist
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf tournamentorganisation.Userlist
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf tournamentorganisation.Userlist
*/
//	onExit: function() {
//
//	}
	
	showUserInDetailTable : function (oEvent){
		oDetailTable = sap.ui.getCore().byId(this.prefix+"userlistDetail_table");
//		oSelectedItem = oEvent.getParameter("listItem");
		oSelectedItem = oEvent.getSource().getSelectedItem();
		oItemPath = oSelectedItem.oBindingContexts.userlistModel.sPath;
		
		oRole = sap.ui.getCore().getModel('userListModel').getProperty(oItemPath).role;
		
	    oDetailTable.bindAggregation("items", {
	    	path : "userListModel>"+oItemPath, //path not what i search .......
	    	template : new sap.m.ColumnListItem({
	    		cells : [new sap.m.Label({
	    					text : oRole
	    				}),
	    				new sap.m.Label({
	    					text : "{userListModel>password}"
	    				})]
	    	})
	    });
	}
});
	//TODO iwann benutzen zum usertable filtern
//	filterUserdetails : function(oEvent){
//		oSelectedItem = oEvent.getParameter("listItem");// "rowContext" is undefined
//		sItemName = oSelectedItem.getBindingContext("userlistModel").getProperty("realName");
//	    oListBinding = sap.ui.getCore().byId(this.prefix+"userlistDetail_table").getBinding("items");
//	    var oFilter = new sap.ui.model.Filter("Name", "EQ", sItemName); //TODO stattdessen id in JSON schreiben
//	    oListBinding.filter([oFilter])
//	},



/*
showNotifications: function() {
    this.clearSearch();
    this.oList = sap.ui.getCore().byId("workOrderListTable");
    var oFilter = new sap.ui.model.Filter("workorderNumber", sap.ui.model.FilterOperator.EQ, "");
    this.oList.getBinding("items").filter(oFilter);
},

showIncidents: function() {
    this.clearSearch();
    this.oList = sap.ui.getCore().byId("workOrderListTable");
    var oFilter = new sap.ui.model.Filter("workorderNumber", sap.ui.model.FilterOperator.NE, "");
    this.oList.getBinding("items").filter(oFilter);
},*/