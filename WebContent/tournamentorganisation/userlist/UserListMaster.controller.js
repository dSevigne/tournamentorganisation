sap.ui.controller("tournamentorganisation.userlist.UserListMaster", {
	prefix : "UserListMaster_",

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf tournamentorganisation.userlist.UserListMaster
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf tournamentorganisation.userlist.UserListMaster
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf tournamentorganisation.userlist.UserListMaster
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf tournamentorganisation.userlist.UserListMaster
*/
//	onExit: function() {
//
//	}
	
	itemSelected : function(){
		var rawSplitApp = sap.ui.getCore().byId("RawSplitApp_rawSplitApp");
		var userListDetailPage = sap.ui.getCore().byId("RawSplitApp_userListDetailPage");
		var userListMaster = sap.ui.getCore().byId(this.prefix+"userListMaster");
		
		var selectedItemMaster = userListMaster.getSelectedItem();
		var selectedItemMasterPath = selectedItemMaster.oBindingContexts.userListModel.sPath;
		var selectedItem = sap.ui.getCore().getModel("userListModel").getProperty(selectedItemMasterPath);
		
		var userListSelectedItemModel = new sap.ui.model.json.JSONModel(selectedItem);
		sap.ui.getCore().setModel(userListSelectedItemModel, "userListSelectedItemModel");
		
		rawSplitApp.toDetail(userListDetailPage);
	}

});