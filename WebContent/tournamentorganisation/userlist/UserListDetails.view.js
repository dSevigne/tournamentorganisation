sap.ui.jsview("tournamentorganisation.userlist.UserListDetails", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf tournamentorganisation.userlist.UserListDetails
	*/ 
	getControllerName : function() {
		return "tournamentorganisation.userlist.UserListDetails";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf tournamentorganisation.userlist.UserListDetails
	*/ 
	createContent : function(oController) {
		var prefix = oController.prefix;
		
		userListDetailsForm = new sap.ui.layout.form.SimpleForm(prefix+"userListDetailsForm", {
			maxContainerCols: 2,
			editable: true,
			content:[
					new sap.ui.core.Title({text:"UserDetails"}),
					new sap.m.Label({text:"Vorname"}),
					new sap.m.Text({text:"{userListSelectedItemModel>/firstName}"}),
					new sap.m.Label({text:"lalalalal"}),
					new sap.m.Text({text:"{tourneyModel>/tourneys/2/name}"})
			]
		});
		

//		userDetailsItemTemplate = new sap.m.StandardListItem({
//			id : prefix+"userDetailsItemTemplate",
//			title : "Titlellll",
//		//	description : "{userListSelectedItemModel>contest}"
//		});
//
//		userListDetailsForm.bindAggregation("items",
//				"userListSelectedItemModel>/", userDetailsItemTemplate);
		
		
		var userListDetailsPage = new sap.m.Page(prefix+"userListDetailsPage",{
			content: [userListDetailsForm],
			footer: [sap.ui.jsfragment("fragments.footer", oController)]
		});
		
 		return userListDetailsPage;
	}

});