sap.ui.jsview("tournamentorganisation.userlist.Userlist", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf tournamentorganisation.Userlist
	*/ 
	getControllerName : function() {
		return "tournamentorganisation.userlist.Userlist";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf tournamentorganisation.Userlist
	*/ 
	createContent : function(oController) {
	    var prefix = oController.prefix;	
	    var sItemName = "";
	
	    var userlist_table = new sap.m.Table(prefix+"userlist_table", {
	        columns: [
	            new sap.m.Column({
	                header: new sap.m.Label({ text: "First Name" }),
	            }),
	            new sap.m.Column({
	            	header: new sap.m.Label({ text: "Last Name" })
	            }),
	            new sap.m.Column({
	            	header: new sap.m.Label({ text: "User Name" })
	            }),
	            new sap.m.Column({
	            	header: new sap.m.Label({ text: "Score" })
	            })
	        ],
	        mode : sap.m.ListMode.SingleSelectMaster,
	        //TODO select:? nachsehen
	        selectionChange : 
	        	function(oEvent) {
	        		oController.showUserInDetailTable(oEvent);
	        	} //oEvent = zusammenstellung von kram und infos, zB wer die funktion
	    			//getriggert hat und parameter bla.... genauer anschauen!
	    });
	    
	    userlist_table.bindAggregation("items", { //WTF is items.. prolly the identifier of the whole aggregation
	        path: "userListModel>/users",
	        template: new sap.m.ColumnListItem({
	            cells: [
	                    new sap.m.Label({ text: "{userListModel>realName}" }),
	                    new sap.m.Label({ text: "{userListModel>userName}" }),
	                    new sap.m.Label({ text: "{userListModel>score}" }),
	                    new sap.m.Label({ text: "{userListModel>active}" })
	                   ]
	        })
	    });    

	    var userlistDetail_table = new sap.m.Table(prefix+"userlistDetail_table",{
			headerText : "Details",
			headerDesign : sap.m.ListHeaderDesign.Standard,
	        columns: [
	  	            new sap.m.Column({
	  	                header: new sap.m.Label({ text: "Role" }),
	  	            }),
	  	            new sap.m.Column({
	  	                header: new sap.m.Label({ text: "Password" }),
	  	            })
	  	        ],
		})
	    
		var userlist_page = new sap.m.Page({
			title: "Userlist",
			content: [userlist_table, userlistDetail_table],
			footer:[sap.ui.jsfragment("fragments.footer", oController)]
		});
 		
	    //TODO page in splitappview oder matrix anordnen?!?!?!??! :D
 		return userlist_page;
	}

});
