sap.ui.jsview("tournamentorganisation.RawSplitApp", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf tournamentorganisation.RawSplitApp
	*/ 
	getControllerName : function() {
		return "tournamentorganisation.RawSplitApp";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf tournamentorganisation.RawSplitApp
	*/ 
	createContent : function(oController) {
	    var prefix = oController.prefix;
 		var rawSplitApp = new sap.m.SplitApp({
 		    id : prefix + "rawSplitApp",
 		    
 		    
 		});

 	     var initialDetailPage = new sap.m.Page({
             id : prefix + "initialDetailPage",
             title: "RawSplitAppDetails",
             content: [new sap.m.Label({text : "Willkommen auf der Detail-Startseite"})],
             footer:[sap.ui.jsfragment("fragments.footer", oController)]
         });
 	     
 	     var userListDetailPage = sap.ui.view(prefix + "userListDetailPage",{
        	 viewName:"tournamentorganisation.userlist.UserListDetails",
        	 type:sap.ui.core.mvc.ViewType.JS
         });
 	     
 	     var initialMasterPage = new sap.m.Page({
             id : prefix + "initialMasterPage",
             title: "RawSplitAppMaster",
             content: []
         });
         
 	     var userListMasterPage = sap.ui.view(prefix + "userListMasterPage",{
        	 viewName:"tournamentorganisation.userlist.UserListMaster",
        	 type:sap.ui.core.mvc.ViewType.JS
         });
 	     
 		rawSplitApp.addDetailPage(initialDetailPage).addDetailPage(userListDetailPage);
 		rawSplitApp.addMasterPage(initialMasterPage).addMasterPage(userListMasterPage);

 		rawSplitApp.setInitialDetail(prefix+"initialDetailPage");
 		rawSplitApp.setInitialMaster(prefix+"initialMasterPage");
 		
 		return rawSplitApp;
	}
	

});