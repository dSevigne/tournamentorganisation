sap.ui.controller("tournamentorganisation.InitialView", {
    prefix: "InitialView_",
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf tournamentorganisation.InitialView
*/
	onInit: function() {
		//load globally used models here
		
		var userListModel = new sap.ui.model.json.JSONModel("models/users.json");
	    sap.ui.getCore().setModel(userListModel, "userListModel");
	    
		var tourneyModel = new sap.ui.model.json.JSONModel("models/tourneys.json");
	    sap.ui.getCore().setModel(tourneyModel, "tourneyModel");
    
		var equipmentsModel = new sap.ui.model.json.JSONModel("models/getEquipments.json");
	    sap.ui.getCore().setModel(equipmentsModel, "equipmentsModel");
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf tournamentorganisation.InitialView
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf tournamentorganisation.InitialView
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf tournamentorganisation.InitialView
*/
//	onExit: function() {
//
//	}

    navigateToUserlist : function(){       
        var hashChanger = new sap.ui.core.routing.HashChanger();
        hashChanger.setHash("userlist");
    },
    
    navigateToUserListMaster : function(){       
        var hashChanger = new sap.ui.core.routing.HashChanger();
        hashChanger.setHash("userlistmaster");
    },

    navigateToRawSplitApp : function(){
        var hashChanger = new sap.ui.core.routing.HashChanger();
        hashChanger.setHash("rawSplitApp");
    },
    
    navigateToCurrentTourneys : function(){
        var hashChanger = new sap.ui.core.routing.HashChanger();
        hashChanger.setHash("currentTourneys");
    },
});