sap.ui.jsview("tournamentorganisation.tourneys.TourneyMaster", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf tournamentorganisation.tourneys.TourneyMaster
	*/ 
	getControllerName : function() {
		return "tournamentorganisation.tourneys.TourneyMaster";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf tournamentorganisation.tourneys.TourneyMaster
	*/ 
	createContent : function(oController) {
		var prefix = oController.prefix;
		tourneyMasterList = new sap.m.List({
			id : prefix+"tourneyMasterList",
			mode : sap.m.ListMode.SingleSelectMaster,
			select : function(oEvent){
				oController.itemSelected(oEvent.getSource());
			}
		});
		
		tourneyMasterItemTemplate = new sap.m.StandardListItem({
			id : prefix+"tourneyMasterItemTemplate",
			title : "{tourneyModel>name}",
			description : "{tourneyModel>contest}"
		});

		tourneyMasterList.bindAggregation("items",
				"tourneyModel>/tourneys", tourneyMasterItemTemplate);
		
		
		tourneyMasterPage = new sap.m.Page({
			title : "Laufende Turniere",
			content : [tourneyMasterList]
		});
		
 		return tourneyMasterPage;
	}

});