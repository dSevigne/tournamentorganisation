sap.ui.jsview("tournamentorganisation.InitialView", {
    

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf tournamentorganisation.InitialView
	*/ 
	getControllerName : function() {
		return "tournamentorganisation.InitialView";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf tournamentorganisation.InitialView
	*/ 
	createContent : function(oController) {
        var prefix = oController.prefix;
        var title = new sap.m.Title({
            id : prefix + "title",
            text : "Overview"
        });
        
        //TODO username text als user referenzieren
        var username = new sap.m.Text({
            id : prefix+"username",
            text : "Till"
        });
        
        //TODO onPress function
        var tile_userlist = new sap.m.StandardTile({
            id : prefix+"tile_userlist",
            title : "Userlist",
            press : function() {oController.navigateToUserListMaster();}
        });
        
        //TODO onPress function
        var tile_currentTourneys = new sap.m.StandardTile({
            id : prefix+"tile_currentTourneys",
            title : "Tourneys",
            press : function() {oController.navigateToCurrentTourneys();}
        }); 
        
        //TODO onPress function
        var tile_sampleView = new sap.m.StandardTile({
            id : prefix+"tile_sampleView",
            title : neusta.title,
            press : function() {neusta.neustafunction();}
        });
        
        //TODO onPress function
        var tile_sampleView2 = new sap.m.StandardTile({
            id : prefix+"tile_sampleView2",
            title : "SampleTile",
            press : function() {parseJsonToXml("sampleViewToast");}
        });  
        
        var tile_sampleView3 = new sap.m.StandardTile({
            id : prefix+"tile_sampleView3",
            title : "showRawSplitApp",
            press : function() {oController.navigateToRawSplitApp();}
        });
        
        var oTile_container = new sap.m.TileContainer({
            id : prefix+"oTile_container",
            tiles : [tile_userlist, tile_currentTourneys, tile_sampleView, tile_sampleView2, tile_sampleView3]
        });
        
    //   var footer = sap.ui.jsfragment("fragments.footer", oController)

        var entryPage = new sap.m.Page({
            id : prefix+"entryPage", 
            customHeader : new sap.m.Bar({
                contentRight : [username],
                contentMiddle : [title]
            }),
            content : [oTile_container],
            footer : [sap.ui.jsfragment("fragments.footer", oController)]
        });
        
        return entryPage;
    }
	
	
//    var page2 = new sap.m.Page("page2", {
//        title: "Page 2",
//        showNavButton: true,       // page 2 should display a back button
//        navButtonPress: function(){ 
//            app.back();            // when pressed, the back button should navigate back up to page 1
//        },
//        content : new sap.m.Text({text:"Hello Mobile World!"})
//    });

});